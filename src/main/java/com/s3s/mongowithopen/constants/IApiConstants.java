package com.s3s.mongowithopen.constants;

public interface IApiConstants {
	
	public static final String APPLICATION_JSON = "application/json";
	public static final String RESPONSECODE_200 = "200";
	public static final String RESPONSE_200 = "OK";
	public static final String RESPONSECODE_400 = "400";
	public static final String RESPONSE_400 = "Bad Request";
	public static final String RESPONSECODE_500 = "500";
	public static final String RESPONSE_500 = "Internal Server Error";
	
	

}
