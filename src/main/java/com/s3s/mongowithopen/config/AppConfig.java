package com.s3s.mongowithopen.config;

import java.sql.JDBCType;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.client.RestTemplate;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;

@Configuration
@EnableAsync
public class AppConfig {
	
	@Bean
	public OpenAPI openApi() {
		return new OpenAPI().components(new Components()).info(new Info().title("Employee Service API"));
	}
	


}
