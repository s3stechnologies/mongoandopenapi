package com.s3s.mongowithopen.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.s3s.mongowithopen.model.Employee;


@Repository
public interface EmployeeRepo extends MongoRepository <Employee, Integer>{
	
	public List<Employee> getEmployeeByFirstName(String firstName);
	
	

}
