package com.s3s.mongowithopen.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import com.s3s.mongowithopen.model.Employee;
import com.s3s.mongowithopen.repository.EmployeeRepo;

@Service
public class EmployeeServiceImpl {
	
	
	@Autowired
	private EmployeeRepo repository;
	
	
	
	
	
	public void saveEmployee(List<Employee> employee) {
		
		repository.saveAll(employee);
		
		
	}


	public List<Employee> getEmployee() {
		
		return repository.findAll();
	}
	
	
	
	public List<Employee> getEmployeeByFirstName(String firstName) {
		
		
		
		return repository.getEmployeeByFirstName(firstName);
	}
	
}
