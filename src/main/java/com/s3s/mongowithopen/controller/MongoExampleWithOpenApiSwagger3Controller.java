package com.s3s.mongowithopen.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.s3s.mongowithopen.constants.IApiConstants;
import com.s3s.mongowithopen.model.Employee;
import com.s3s.mongowithopen.service.EmployeeServiceImpl;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

import lombok.extern.log4j.Log4j2;

@RestController
@Log4j2
@CrossOrigin
public class MongoExampleWithOpenApiSwagger3Controller {
	
	@Autowired 
	private EmployeeServiceImpl service;
	
	
	@Operation(summary = "Save Emplyee Data", description = "This method is used to save Employee data in DB! ", 
			requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody (required = true, content = @Content(schema = @Schema(implementation = Employee.class),
			mediaType = IApiConstants.APPLICATION_JSON)), responses = {
			@ApiResponse(responseCode = IApiConstants.RESPONSECODE_200,description = IApiConstants.RESPONSE_200, content = @Content(array = @ArraySchema(schema = @Schema(implementation = Employee.class)), 
			             mediaType = IApiConstants.APPLICATION_JSON)),
			@ApiResponse(responseCode = IApiConstants.RESPONSECODE_500, description = IApiConstants.RESPONSE_500),
			@ApiResponse(responseCode = IApiConstants.RESPONSECODE_400, description = IApiConstants.RESPONSE_400)})
	@PostMapping(value = "api-service/employee/v1/save", produces = IApiConstants.APPLICATION_JSON)
	public ResponseEntity saveEmployeeData(@RequestBody(required = true) List<Employee> employee) {
		
		ResponseEntity response = null;
		
		try {
			service.saveEmployee(employee);
			response = new ResponseEntity(HttpStatus.OK);
			
		}catch(Exception e) {
			//log.info("Error occurred while processing!", e.getMessage());
			response = new ResponseEntity("Error occured while processing!", HttpStatus.BAD_REQUEST);
			
		}
		return response;
	}
	
	
	@Operation(summary = "Get Emplyee Data", description = "This method is used to get Employee data from DB! ", 
//			requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody (required = true, content = @Content(schema = @Schema(implementation =String.class),
//			mediaType = IApiConstants.APPLICATION_JSON)), 
			responses = {
			@ApiResponse(responseCode = IApiConstants.RESPONSECODE_200,description = IApiConstants.RESPONSE_200, content = @Content(array = @ArraySchema(schema = @Schema(implementation = Employee.class)), 
			             mediaType = IApiConstants.APPLICATION_JSON)),
			@ApiResponse(responseCode = IApiConstants.RESPONSECODE_500, description = IApiConstants.RESPONSE_500),
			@ApiResponse(responseCode = IApiConstants.RESPONSECODE_400, description = IApiConstants.RESPONSE_400)})
	@GetMapping(value = "api-service/employee/v1/getByFirstName", produces = IApiConstants.APPLICATION_JSON)
	public ResponseEntity getAllEmployee(String firstName){
		
			ResponseEntity response = null;
		
		try {
		
			response = new ResponseEntity(service.getEmployeeByFirstName(firstName),HttpStatus.OK);
			
		}catch(Exception e) {
			//log.info("Error occurred while processing!", e.getMessage());
			response = new ResponseEntity("Error occured while processing!", HttpStatus.BAD_REQUEST);
			
		}
		return response;
		
	}
	
	
	@Operation(summary = "Get Emplyee Data", description = "This method is used to get Employee data from DB! ", 
//			requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody (required = true, content = @Content(schema = @Schema(implementation =String.class),
//			mediaType = IApiConstants.APPLICATION_JSON)), 
			responses = {
			@ApiResponse(responseCode = IApiConstants.RESPONSECODE_200,description = IApiConstants.RESPONSE_200, content = @Content(array = @ArraySchema(schema = @Schema(implementation = Employee.class)), 
			             mediaType = IApiConstants.APPLICATION_JSON)),
			@ApiResponse(responseCode = IApiConstants.RESPONSECODE_500, description = IApiConstants.RESPONSE_500),
			@ApiResponse(responseCode = IApiConstants.RESPONSECODE_400, description = IApiConstants.RESPONSE_400)})
	@GetMapping(value = "api-service/employee/v1/getAll", produces = IApiConstants.APPLICATION_JSON)
	public ResponseEntity getAllEmployee(){
		
			ResponseEntity response = null;
		
		try {
		
			response = new ResponseEntity(service.getEmployee(),HttpStatus.OK);
			
		}catch(Exception e) {
			//log.info("Error occurred while processing!", e.getMessage());
			response = new ResponseEntity("Error occured while processing!", HttpStatus.BAD_REQUEST);
			
		}
		return response;
		
	}

}
